//
//  ViewController.m
//  手写代码创建控件
//
//  Created by 廖家龙 on 2020/4/19.
//  Copyright © 2020 liuyuecao. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

//当要显示一个界面的时候，首先创建这个界面对应的控制器，控制器创建好以后，接着创建控制器所管理的那个view，当这个view加载完毕以后就开始执行下面的方法了
//所以只要viewDidLoad方法被执行了，就表示控制器所管理的view创建好了
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //动态创建自己的按钮
    
    //1.创建按钮（UIButton）
    
      //1) UIButton *button=[[UIButton alloc]init];
      //2) 创建系统默认的按钮，也可以创建Custom的按钮
    UIButton *button=[UIButton buttonWithType:UIButtonTypeSystem];
    
    //2.设置按钮上显示的文字
    
      //默认状态下显示的文字
    [button setTitle:@"第一张图片" forState:UIControlStateNormal];
      //高亮状态下显示的文字
    [button setTitle:@"第二张图片" forState:UIControlStateHighlighted];
    
    //3.设置不同状态下的文字颜色
    
      //默认状态下设置为红色
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
      //高亮状态下设置为蓝色
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    
    //4.设置按钮的背景图片
    
      //加载图片：一个UIImage对象代表一张图片，一般通过imageNamed:方法就可以通过文件名加载项目中的图片
    UIImage *imgNormal=[UIImage imageNamed:@"图片1.png"];
    UIImage *imgHighlighted=[UIImage imageNamed:@"图片2.png"];
    
      //设置默认状态下的背景图片
    [button setBackgroundImage:imgNormal forState:UIControlStateNormal];
      //设置高亮状态下的背景图片
    [button setBackgroundImage:imgHighlighted forState:UIControlStateHighlighted];
    
    //5.设置按钮的frame（x=50，y=100，这个按钮的宽度为110，高度为120）
    button.frame=CGRectMake(50, 100, 110, 120);
    
    //通过代码为按钮注册一个单击事件
    [button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    
    //6.把动态创建的按钮加到控制器所管理的那个view中
    [self.view addSubview:button];
    
}

- (void)buttonClick{
    NSLog(@"按钮被点击了。");
}

@end
